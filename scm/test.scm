#!/usr/bin/guile \
-e main
!#
;; Copyright (c) 2018 Thomas Lingefelt

;; Permission is hereby granted, free of charge, to any person
;; obtaining a copy of this software and associated documentation
;; files (the "Software"), to deal in the Software without
;; restriction, including without limitation the rights to use, copy,
;; modify, merge, publish, distribute, sublicense, and/or sell copies
;; of the Software, and to permit persons to whom the Software is
;; furnished to do so, subject to the following conditions:

;; The above copyright notice and this permission notice shall be
;; included in all copies or substantial portions of the Software.

;; THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;; EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;; MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
;; NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS
;; BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
;; ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
;; CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
;; SOFTWARE.

(format #t "This is the file that's getting whatever!")

;; Callback intended to respon to the click event.
(define (on-button-clicked)
  ;; set-label should be defined in the shared library
  ;; or by the libguile ran from the executable binary
  (set-label
   (format #f "(on-button-clicked): ~a" (getpid))))

;; If the source is the main entry point
(define (main args)
  ;; Set this here for visual differentiation.
  (set! on-button-clicked
        (lambda ()
          (set-label "Alternative to (on-button-clicked) called")))
  (load-extension "build/libguile-gtk-testing.so" "scm_init_lib")
  (run-ui))
