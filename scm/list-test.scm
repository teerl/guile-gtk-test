#!/usr/bin/guile \
-e main
!#

(define (main args)
  (load-extension "build/libguile-gtk-list-test.so" "init_gtk_list_test")
  (let loop ((x 10))
    (add-item "To Be Honest" #t)
    (if (> x 0) (loop (1- x))))
  (run-gui))
