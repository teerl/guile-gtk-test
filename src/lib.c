/* Copyright (c) 2018 Thomas Lingefelt

   Permission is hereby granted, free of charge, to any person obtaining
   a copy of this software and associated documentation files (the
   "Software"), to deal in the Software without restriction, including
   without limitation the rights to use, copy, modify, merge, publish,
   distribute, sublicense, and/or sell copies of the Software, and to
   permit persons to whom the Software is furnished to do so, subject to
   the following conditions:

   The above copyright notice and this permission notice shall be
   included in all copies or substantial portions of the Software.

   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
   EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
   MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
   NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
   LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
   OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
   WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE. */


/* Testing Library File */
#include <gtk/gtk.h>
#include <libguile.h>


/* Please no bully */
static GtkLabel *label = NULL;

/* Exported procedure for Guile source to set the label on the
   window */
static SCM set_label(SCM msg)
{
    const char* c_msg = scm_to_locale_string(msg);
    gtk_label_set_text(label, c_msg);
    return SCM_BOOL_T;
}

static void wrapper_on_button_clicked(GtkLabel *l)
{
    scm_c_eval_string("(on-button-clicked)");
}

/* Exported procedure.
   Only returns once the GUI window has been
   deleted.
   Run from Guile source if it's the main entry point.
   Run inside main() if C is the main entry. */
void run_ui()
{
    /* Standard GTK stuff */
    gtk_init(0, NULL);
    GtkBuilder *builder = gtk_builder_new_from_file("ui/ui.glade");
    GtkWindow *window = (GtkWindow*)gtk_builder_get_object(builder, "window");
    label = (GtkLabel*)gtk_builder_get_object(builder, "label");
    GtkButton *button = (GtkButton*)gtk_builder_get_object(builder, "button");
    g_object_unref(builder);
    /* Setting callbacks manually because my verson of glade-3 doesn't work with
       signals properly */
    g_signal_connect(window, "delete-event", G_CALLBACK(gtk_main_quit), NULL);
    g_signal_connect_swapped(button, "clicked", G_CALLBACK(wrapper_on_button_clicked), label);
    gtk_widget_show(GTK_WIDGET(window));
    gtk_main();
}

/* NOT EXPORTED
   This does initialization that only C needs. */
void c_init_lib()
{
    /* Right now just define (set-label) */
    scm_c_define_gsubr("set-label", 1, 0, 0, set_label);
}

/* Exported procedure.
   Initializes the library when scheme
   is the main entry point */
void scm_init_lib()
{
    c_init_lib();
    scm_c_define_gsubr("run-ui", 0, 0, 0, run_ui);
}
