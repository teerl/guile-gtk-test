/* Copyright (c) 2018 Thomas Lingefelt */

/* Permission is hereby granted, free of charge, to any person obtaining */
/* a copy of this software and associated documentation files (the */
/* "Software"), to deal in the Software without restriction, including */
/* without limitation the rights to use, copy, modify, merge, publish, */
/* distribute, sublicense, and/or sell copies of the Software, and to */
/* permit persons to whom the Software is furnished to do so, subject to */
/* the following conditions: */

/* The above copyright notice and this permission notice shall be */
/* included in all copies or substantial portions of the Software. */

/* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, */
/* EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF */
/* MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND */
/* NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE */
/* LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION */
/* OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION */
/* WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE. */

#include <gtk/gtk.h>
#include <libguile.h>

enum
{
    COL_B,
    COL_S,
    N_COL
};

GtkListStore *liststore = NULL;

static void row_activated(GtkTreeModel *model, GtkTreePath *path) 
{
    g_print("row-activated\n");
    GtkTreeIter iter;
    gtk_tree_model_get_iter(model, &iter, path);
    gboolean b;
    gtk_tree_model_get(model, &iter, COL_B, &b, -1);
    gtk_list_store_set(GTK_LIST_STORE(model), &iter, COL_B, b ? FALSE : TRUE, -1);
}

static GtkListStore *make_the_list_store()
{
}


static GtkTreeView *make_the_tree_view(GtkTreeModel *model)
{
    GtkTreeView *tview = (GtkTreeView*)gtk_tree_view_new_with_model(model);
    GtkCellRenderer *renderer;
    renderer = gtk_cell_renderer_text_new();
    gtk_tree_view_insert_column_with_attributes(tview,
                                                -1,
                                                "S",
                                                renderer,
                                                "text", COL_S,
                                                NULL);                                               
    renderer = gtk_cell_renderer_toggle_new();
    gtk_cell_renderer_toggle_set_activatable(GTK_CELL_RENDERER_TOGGLE(renderer), FALSE);
    gtk_tree_view_insert_column_with_attributes(tview,
                                                -1,
                                                "Bool",
                                                renderer,
                                                "active", COL_B,
                                                NULL);
    gtk_tree_view_set_headers_visible(tview, TRUE);
    gtk_tree_view_set_activate_on_single_click(tview, TRUE);
    
    return tview;
}

static void activate(GtkApplication* app, gpointer user_data)
{
    GtkWidget *window;
    window = (GtkWidget*)gtk_application_window_new(app);

    GtkTreeView *tview = make_the_tree_view(GTK_TREE_MODEL(liststore));
    g_signal_connect_swapped(G_OBJECT(tview), "row-activated",
                             G_CALLBACK(row_activated), liststore);
    gtk_container_add(GTK_CONTAINER(window), GTK_WIDGET(tview));
    
    gtk_widget_show_all(window);
}

SCM add_item(SCM item, SCM checked)
{
    GtkTreeIter iter;
    gtk_list_store_append(GTK_LIST_STORE(liststore), &iter);
    gtk_list_store_set(GTK_LIST_STORE(liststore), &iter,
                       COL_B, scm_to_bool(checked),
                       COL_S, scm_to_locale_string(item),
                       -1);
    return SCM_BOOL_T;
}

SCM run_gui(int argc, char **argv)
{
    GtkApplication *app = gtk_application_new("this.is.a.test", G_APPLICATION_FLAGS_NONE);
    g_signal_connect(app, "activate",
                     G_CALLBACK(activate), NULL);
    g_application_run(G_APPLICATION(app), 0, NULL);
    g_object_unref(app);
    return SCM_BOOL_T;
}


void init_gtk_list_test()
{
    liststore = gtk_list_store_new(N_COL, G_TYPE_BOOLEAN, G_TYPE_STRING);
    scm_c_define_gsubr("run-gui", 0, 0, 0, run_gui);
    scm_c_define_gsubr("add-item", 2, 0, 0, add_item);
}
